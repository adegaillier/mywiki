# Wiki and suffs

This repo contains all tips, examples, how to that I think worth it.

## Software
* [Piprinter](piprinter/Raspberry-pi-zero_Raspbian-buster-lite_and_cups.md)
* [Linux tips](Linux-tips/linux-tips.md)
* [Home Server](home-server/dumnas.md)
* [Pipenv](python/pipenv.md)
* [Markdown cheatsheet](markdown/cheatsheet.md)
* Ocotprint
    * [Installation](octoprint/installation.md)
    * [Multicam](octoprint/multicam.md)

## Hardware

* [Home system overview](hardware/home-system/home-system.md)
