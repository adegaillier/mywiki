# Markdown cheatsheet

### Diff integration

```diff
diff --git a/projects/belrob/rootfs/usr/bin/brsysglue.sh b/projects/belrob/rootfs/usr/bin/brsysglue.sh
index ff2c63ca82..9420671d02 100755
--- a/projects/belrob/rootfs/usr/bin/brsysglue.sh
+++ b/projects/belrob/rootfs/usr/bin/brsysglue.sh
@@ -7,6 +7,8 @@ ICCIDFILE=/etc/ppp/config/iccid

 MQTTNAME=brcanspecial-$$

+ODIN_AT_MODE_ENABLED=0
+
 run()
 {
 	echo "$ $@" >&2
@@ -48,6 +50,7 @@ mosquitto_sub -i "$MQTTNAME-core-sub" -v \
 	-t 'net/ppp0/+/set' \
 	-t 'net/tun0/+' \
 	-t 'trig/#' \
+	-t 'odin/state' \
 	| while read TOPIC PAYLOAD; do
 if [ "$PAYLOAD" = "(null)" ]; then
 	# remove this mosquitto_sub fancy output
@@ -209,6 +212,7 @@ trig/stop-odintomqtt)
 	else
 		runcl stop odintomqtt-cmd
 	fi
+	ODIN_AT_MODE_ENABLED=0
 	# Set ODIN-W2 comm to ODIN -> ZED
 	echo 1 > /sys/class/leds/odin/brightness
 	;;
@@ -224,6 +228,13 @@ trig/stop-ubxtomqtt)
 	# Resume nmea0183tomqtt
 	runcl resume nmea0183tomqtt-ubx
 	;;
+odin/state)
+	if [ "$PAYLOAD" = "ready" ]; then
+		# Odin has restarted and switch in AT mode
+		ODIN_AT_MODE_ENABLED=1
+		echo "Odin has switched in AT mode" > /dev/console
+	fi
+	;;
 trig/reboot-rtkubx)
 	{
 		# Set ODIN-W2 comm to ODIN -> MainBoard
@@ -231,10 +242,18 @@ trig/reboot-rtkubx)

 		runcl start odintomqtt-cmd
 		# Waiting for Odin to switch in AT mode
-		sleep 10
-		mosquitto_pub -t odin/raw/send -m "AT+CPWROFF"
-		sleep 1
+		iteration=0
+		while [ "$ODIN_AT_MODE_ENABLED" != "1" -a iteration -lt 10 ]; do
+			echo "Wait for odin to switch in AT mode" > /dev/console
+			sleep 1
+			iteration=$((iteration+1))
+		done
+		if [ "$ODIN_AT_MODE_ENABLED" = "1" ]; then
+			mosquitto_pub -t odin/raw/send -m "AT+CPWROFF"
+			sleep 1
+		fi
 		runcl stop odintomqtt-cmd
+		ODIN_AT_MODE_ENABLED=0

 		# Set ODIN-W2 comm to ODIN -> ZED
 		echo 1 > /sys/class/leds/odin/brightness
```


## Tables

### Simple
|                             | Position 1 | Position 2 | Position 3 |
|-----------------------------|------------|------------|-----------|
| Distance from RTK base [m]  | 270  | 280  | 422         |
| RSSI value on Robot [dBm]   | -77  | -78  | -80         |
| Connection quality | Good | Good  | Poor but ok |
| RTK base power output [dBm] | 10   | 10   | 10          |

### With image

| Presence | Magnetic ch-4 | Obstacle |
| ------ | ------ | ------ |
| Resolution : 0.20 <br> Width: 750  <br> Height: 750 | Resolution : 1.00 <br> Width: 100  <br> Height: 100 | Resolution : 2.00 <br> Width: 100  <br> Height: 100 |
| <img src="/uploads/0992a5d71e3ae9cbc2013be84a136ce1/presence_map.png"  width="600"> | <img src="/uploads/44573e61b0d39df6b8577eb0c41c9ea1/magnetic-ch4_map.png"  width="600"> | <img src="/uploads/b7f1ab9dfd36d45358f7c179b1c6bfaf/obstacles_map.png"  width="600"> |

### With bullet point and list

| Main FSM states | LEDs state | Entrer/Exit conditions |
| ------ | ------ | ----------------------------------- |
| Initialization | - Main LEDS: Red and Green leds blink alternatively - 1hz <br> <br> - Config LED: **x** | <ul><li>Enter: <ul><li> Power on the RTK base </li></ul> </li> <li>Exit: <ul><li> All module initialized successfully </li><li> Error detected </li></ul> </li></ul> |

## Images

<img src="/uploads/3e0189430a13394715564979489edd51/IMG_20201202_111456.jpg"  height="400">

