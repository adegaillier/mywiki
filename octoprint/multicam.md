# Multicam configuration 

[[_TOC_]]

## Use cases covered

### Hadrware supported

## How opctoprint use USB webcam

1. Detect device by drivers /dev/videoi
2. Service webcamd.service start a video streamer based on config in /boot/octoprint.txt
3. webcamd.service is a simple shell script that process the /boot/octoprint.txt and the available devices to start the streamer adapted.

-> This script could be modify to handle several available input. See section [Thoughts - Generalize for n webcam ](#Thoughts:-Generalize-for-n-webcams )

## Add one webcam stream on /webcam2/ proxy

## Thoughts: Generalize for n webcams