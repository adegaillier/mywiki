# Linux tips

## Automount Samba server location
1. Edit /etc/fstab and add entry of the corresponding location.

Example:
```
#automount dumbnas samba server if available (only for grp Adrien and user Adrien)
//dumnas/dumnas /media/adrien/dumnas cifs rw,_netdev,user=adrien,password=bananapi,uid=1000,gid=1000 0 0
```

Note: in this example, the hostname of the NAS is dumnas, samba fileshare is dumnas
2. reboot

## Create a read-only view of a directory in a different location
```
mkdir -p <dir-readonly-location>/new_directory
chown <user>:<group> <dir-readonly-location>
chmod 750 <dir-readonly-location>

bindfs -p a-w <dir-location> <dir-readonly-location>/new_directory
```

To create the read-only view at boot time, add the following line ot /etc/fstab

```
bindfs#<dir-location> <dir-readonly-location>/new_directory fuse perms=a=rX
```

## Zeroconf: [Octopi context]

### Linux
*Zeroconf (aka Bonjour, very rarely Rendezvous) is a group of technologies to “automagically” discover systems and services on a local area network.*

Zeroconf helps by assigning the system a name (e.g. raspberrypi.local instead of 192.168.0.42). It can then be easily accessed from other computers on the local network…provided they’re also running Zeroconf! It’s needed at both ends.

Zeroconf is provided through an optional package called Avahi. It’s super easy to install from the command line:

```console
sudo apt-get install avahi-daemon
```
Once installed, the system can be contacted from other computers at hostname.local.

### Windows
Windows doesn’t have Zeroconf support out of the box, but a few popular applications slip it in for their own needs, including Skype, Apple’s iTunes and Adobe Photoshop CS3 or later. So you might not need to add anything at all!

Otherwise, it’s most easily installed using [Bonjour Print Services for Windows 2.0.2](https://support.apple.com/kb/DL999?locale=fr_BE).

The newest-and-shiniest version 3.0 is only available in the iTunes installer. So one option is to simply install iTunes, even if you don’t plan to use it.

Some users are understandably reluctant to install unneeded software. In that case, Bonjour 3 can still be installed with a little trick: download the iTunes installer but don’t run it. Using an archive utility like 7-Zip or WinRAR, you’ll discover there’s a separate Bonjour installer inside. Just extract and run that one piece, and you’re done!

Once installed, Zeroconf systems on the local network can then be accessed by name instead of numbers…in a web browser, for example, one might reach a Raspberry Pi hosting OctoPrint at http://octopi.local


**Ref**
[https://learn.adafruit.com/bonjour-zeroconf-networking-for-windows-and-linux/overview](https://learn.adafruit.com/bonjour-zeroconf-networking-for-windows-and-linux/overview)

## Useful basic command line

- **PWD**
```console
$ pwd
/home/adrien/
```
Print current location