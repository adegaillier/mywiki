# Home system hardware
This document list all HW devices for the different application and services used in the Home system implementation.

It will also put in perspective the current implementation based on the following criteria:

- Long term support firmware
- Performances
- Cost

## Overview

<img src="img/home-system-overview.png">


## Game and design computer
This computer runs Windows 10. It should have enough performance to be able to run AAA games, stream games over Steam, run heavy Solidworks projects, Adobe creative suite (Premiere, photoshop, InDesign, etc) and be able to run some simulation with Gazebo and ROS.

### Current hardware

- Motherboard: P8Z77-V LK: https://www.asus.com/ca-fr/Motherboards/P8Z77V_LK/specifications/
- CPU: Intel core i5-3570k @3.4 Ghz (Quad core)
- Power supply: Raider RA 650 (650W)
- Ram memory: Kingston HyperX Genesis 8 GO (KIT 2X 4 GO) DDR3-SDRAM PC3-12800 CL9
- DVD reader/writer: ATAPI IHAS124
- GPU: ASUS GTX660 TI-DC2-2GD5 2 GB

Current performances are too tight for the AAA games in High quality (target: Mafia 3 >60fps in High quality) and for streaming (Encoding delay of more than 1 seconds). Increasing the CPU and GPU performances and RAM memory is a minimum to have a decent experience. Which mean Upgrading all the Hardware...

### Alternatives

Change everything -> Ask for François Dederich advices

## Ubuntu server - DUMNAS
Raspberry pi4 model B 4Go: https://www.raspberrypi.org/products/raspberry-pi-4-model-b/

- Broadcom BCM2711, Quad core Cortex-A72 (ARM v8) 64-bit SoC @ 1.5GHz
- 2GB, 4GB or 8GB LPDDR4-3200 SDRAM (depending on model)
- 2.4 GHz and 5.0 GHz IEEE 802.11ac wireless, Bluetooth 5.0, BLE
- Gigabit Ethernet
- 2 USB 3.0 ports; 2 USB 2.0 ports.
- 2 × micro-HDMI ports (up to 4kp60 supported)
- 4-pole stereo audio and composite video port
- H.265 (4kp60 decode), H264 (1080p60 decode, 1080p30 encode)
- Micro-SD card slot for loading operating system and data storage
- 5V DC via USB-C connector (minimum 3A*)
- 5V DC via GPIO header (minimum 3A*)
- Operating temperature: 0 – 50 degrees C ambient

\* A good quality 2.5A power supply can be used if downstream USB peripherals consume less than 500mA in total.

## Media center
Should be able to access to a local NAS or local network location and manage movies and TV show database to display the media content of the NAS on a TV. It should also be able to play playlist of live TV access and manage VPN connection.

### Current hardware
Raspberry pi3 model B+: https://www.raspberrypi.org/products/raspberry-pi-3-model-b-plus/

- Broadcom BCM2837B0, Cortex-A53 (ARMv8) 64-bit SoC @ 1.4GHz
- 1GB LPDDR2 SDRAM
- 2.4GHz and 5GHz IEEE 802.11.b/g/n/ac wireless LAN, Bluetooth 4.2, BLE
- Gigabit Ethernet over USB 2.0 (maximum throughput 300 Mbps)
- Full-size HDMI
- 4 USB 2.0 ports
- 4-pole stereo output and composite video port
- Micro SD port for loading your operating system and storing data
- 5V/2.5A DC power input

Run OSMC that manage Kodi application.

### Alternatives
- Raspberry pi 4 ~100€: OpenElec + Kodi https://www.raspberrypi.org/products/raspberry-pi-4-model-b/
- Vero 4k+ ~ 110€: OSMC https://osmc.tv/2018/07/vero-4k-is-here/
- Nvidia shield TV pro ~200€: Kodi + smart IPTV https://www.nvidia.com/fr-fr/shield/shield-tv-pro/

## Game streaming
To be able to stream games from host computer over LAN and/or access to a online stream service.

### Current hardware
Steam link (End of life) : https://www.wikiwand.com/en/Steam_Link

- Purchased price: 50€
- Connectivity: 100 Mbit/s Fast Ethernet and Wireless 802.11ac 2×2 (MIMO)
- 3× USB 2.0 ports
- Bluetooth 4.0
- Support for the following control peripherals:
    - Steam Controller,
    - DualShock 4,
    - Xbox One or 360 Wired Controller,
    - Xbox 360 Wireless Controller for Windows,
    - Logitech Wireless Gamepad F710,
    - keyboard and mouse
- Marvell DE3005-A1 CPU
- Marvell WiFi chip 88W8897
- Vivante GC1000 GPU


### Alternatives

- Raspberry pi 4 ~100€: https://www.raspberrypi.org/products/raspberry-pi-4-model-b/
- Nvidia shield TV pro ~200€: https://www.nvidia.com/fr-fr/shield/shield-tv-pro/
    - comparatif: https://www.cnet.com/reviews/nvidia-shield-tv-review/

- Roku Ultra ~90€: https://www.roku.com/en-gb/products/roku-ultra



## Conclusion

There is two possible evolution of the hardware:

1. Game streaming and media center fused
    - Replace Steam link and Raspberry pi 3 by a **Nvidia shield TV pro**
2. Keep Gaming and Media center on separate devices
    - Replace the Raspberry pi 3 by a **Vero 4k+**