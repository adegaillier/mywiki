# Upgrading classical printer to a connected printer with cups

## Hardware
- Starter kit Raspberry pi zero with wifi:
https://www.amazon.fr/gp/product/B07H4XPZKH/ref=ppx_yo_dt_b_asin_title_o00_s00?ie=UTF8&psc=1
- µSD card > 4go
- Adapted USB cable for your printer
- Printer (here tested on Espson SX115)


## Install raspbian buster lite on SD card
1. Download image on: https://www.raspberrypi.org/downloads/raspbian/
2. Use Etcher or equivalent to flash the image on a µSD card: https://www.balena.io/etcher/
3. Configure the network: create wpa_supplicant.conf and copy file into "/SD/boot/"
```
        ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
        update_config=1
        country=BE
        network={
             ssid="<you-SSID>"
             psk="<your-passwod>"
             key_mgmt=WPA-PSK
        }
```
4. add ssh empty file in boot partition to enable ssh at first boot:
    - "touch ssh /sd/boot/" (linux only)
5. Boot on the RPI

**Note**: If you have trouble on network configuration, use a serial cable for the first boot to check and the network configuration of the RPI and enable SSH connection to ease the access of the RPI.

FTDI cable: https://www.elektor.de/ftdi-chip-ttl-232r-3v3-usb-to-uart-cable

Pinout RPI zero : https://learn.adafruit.com/raspberry-pi-zero-creation/give-it-life


## Install and configure cups

1. Connect in ssh to the rpi
2. Install cups -> "sudo apt-get cups"
3. (Optional) Modification of pi user name by piprinter. You must create a new tmp user to permit the modification of the current user (because of the ssh process).
```
    # sudo useradd -m pibis
    # sudo usermod -a -G sudo pibis
    # exit
    # ssh pibis@<ip-address>
    # sudo usermod -l pi piprinter
    # mv /home/pi /home/piprinter
    # exit
    # ssh piprinter@<ip-address>
    # sudo userdel -r pibis
    # sudo userdel -r pi
    # sudo usermod -a -G lpadmin piprinter
```

ref: : https://hepeng.me/changing-username-and-hostname-on-ubuntu/

5. Open /etc/cups/cupsd.conf and modify following sections to allow all local print request.

```
    # Only listen for connections from the local machine
    # Listen localhost:631
    Port 631

    ...

    < Location / >
    # Restrict access to the server...
    Order allow,deny
    Allow @local
    < /Location >

    < Location /admin >
    # Restrict access to the admin pages...
    Order allow,deny
    Allow @local
    < /Location >

    # Restrict access to the configuration files...
    < Location /admin/conf >
    AuthType Default
    Require user @SYSTEM
    Order allow,deny
    Allow @local
    < /Location >
```
6. Ensure that cups is accessible across all network

```
    # sudo cupsctl –remote-any
```

7. restart service

```
    # sudo /etc/init.d/cups restart
```
or
```
    # sudo systemctl restart cups
```

8. Add printer to the RPI cups web interface on "\<local-rpi-ip\>:631"
    - go to admin tab and add the printer. /!\ don't forget to check the "share this printer" box
    - follow the configuration steps

ref:
- https://www.howtogeek.com/169679/how-to-add-a-printer-to-your-raspberry-pi-or-other-linux-computer/
- https://circuitdigest.com/microcontroller-projects/raspberry-pi-print-server


ex: https://192.168.0.26:631/printers/Epson_Stylus_SX110
