# Home Server - DumNas
You can find all configuration files and other resources dedicate for this home server in the following repository: https://gitlab.com/adegaillier/dumnas

For example, you can use the ubuntu LTS server image for the raspberry pi family: https://ubuntu.com/download/raspberry-pi
## Dumnas - Setup static ip address (optional - prefer router mac address assignment)

1. create 01-netcfg.yaml in /etc/netplan
    ```
    sudo touch 01-netcfg.yaml
    ```

2. add the static configuration for the desire interface
    ```
    network:
    version: 2
    renderer: networkd
    ethernets:
    eth0:
        dhcp4: no
        addresses: [192.168.0.28/24]
        gateway4: 192.168.0.254
        nameservers:
        addresses: [8.8.4.4,8.8.8.8]
    ```

3. restart netplan
    ```bach
    sudo netplan apply # (if issue, you can run sudo netplan --debug apply)
    ```


## DUMNAS - minimal requirement

1. create dumnas user

    1.1. Create dumnas user
    ```
    sudo adduser dumnas
    ```

    1.2. Add sumnas to sudo group
    ```
    sudo adduser dumnas sudo
    ```

2. Add your public key to the .ssh/authorized_keys of the new sudo user dumnas

3. Modify hostname in /etc/hostname by "Home server"

4. (when everything is ready) Disable PasswordAuthentication in /etc/ssh/sshd_config

## DUMNAS - Samba (NAS feature)

<!-- TODO: Add user access for Media location and Owncloud location. checkout the following links

- https://www.google.com/search?q=samba+access+drectory+of+www-data&oq=samba+access+drectory+of+www-data&aqs=chrome..69i57.11919j0j7&sourceid=chrome&ie=UTF-8
- https://serverfault.com/questions/745260/how-to-share-apache-var-www-using-samba-server-in-ubuntu
- https://askubuntu.com/questions/244406/how-do-i-give-www-data-user-to-a-folder-in-my-home-folder
  -->

1. Install package
    ```
    sudo apt-get install samba
    ```

2. Add at the end the /etc/samba/smd.conf the following lines to share directories over the network:

    ```
    [Dumnas]
        comment = HDD home shared
        path = /media/
        read only = no
        browsable = yes
        guest ok = yes
    ```

3. Add as much line as the number of USB drives connected to the device in fstab file located /etc/fstab

    ```
    UUID=6CAE4602AE45C4F4 /media/pi/RAID ntfs defaults 0 0
    UUID=AADEEA03DEE9C7A1 /media/pi/Toshiba ntfs defaults 0 0
    ```

**Note:** Getting UUID of the external drives
    ```
    sudo lsblk -f
    ```

## DUMNAS - Transmission

1. Install package
    ```
    sudo apt-get install transmission-daemon
    ```

2. Configure transmission (replace the settings.json in /etc/transmission by the one in the source repo: https://gitlab.com/adegaillier/dumnas)

3. install new web interface (replace /usr/share/transmission/web by the one in the source repo: https://gitlab.com/adegaillier/dumnas)

## Owncloud - Manual installation (last update 2019)

1. Install dumnas minimal

2. Install owncloud with apt:

    - [https://download.owncloud.org/download/repositories/production/owncloud/](https://download.owncloud.org/download/repositories/production/owncloud/)
    - [https://doc.owncloud.com/server/admin_manual/installation/linux_installation.html#add-the-owncloud-repository](https://doc.owncloud.com/server/admin_manual/installation/linux_installation.html#add-the-owncloud-repository)


    2.1. Owncloud recommend to hold the package owncloud-files to avoid any good/bad surprise after an upgrade and recommend manually upgrade the package when need

3. Install apache2

    3.1. sudo apt-get install apache2

    3.2. Check installation on http://\<server-ip-address\>


4. Install php module to work with apache2

    4.1. Add php repository

        add-apt-repository ppa:ondrej/php && apt-get update

    4.2. Install mod_php and all php dependencies. /!\ Currently only php7.3 is compatibly with the latest version of apache2

        apt install libapache2-mod-php7.3 \
            php-imagick php7.3-common php7.3-curl \
            php7.3-gd php7.3-imap php7.3-intl \
            php7.3-json php7.3-mbstring php7.3-mysql \
            php-ssh2 php7.3-xml php7.3-zip \
            php-apcu php-redis redis-server

5. Create a helper script to simplify running occ commands.

    5.1. create script /usr/local/bin/occ to simplify running occ commands

    ```
    #! /bin/bash

    cd /var/www/owncloud
    sudo -u www-data /usr/bin/php /var/www/owncloud/occ "\$@"
    ```

    5.2. Make helper script executable:

    ```
    chmod +x /usr/local/bin/occ
    ```

6. Install recommended package

```
apt install \
  ssh bzip2 rsync curl jq \
  inetutils-ping smbclient\
  php-smbclient coreutils php7.3-ldap
```

6. Install database manager (MariaDB or MySQL)

```
apt install mariadb-server
```

6. Install openssl

```
apt install openssl
```

7. **Configure Apache**

    7.1. Change the Document Root

    ```
    sed -i "s#html#owncloud#" /etc/apache2/sites-available/000-default.conf

    systemctl restart apache2
    ```

    7.2. Create a Virtual Host Configuration in /etc/apache2/sites-available/owncloud.conf

    ```
    Alias /owncloud "/var/www/owncloud"

    <Directory /var/www/owncloud>
    Options +FollowSymlinks
    AllowOverride All

    <IfModule mod_dav.c>
    Dav off
    </IfModule>

    SetEnv HOME /var/www/owncloud
    SetEnv HTTP_HOME /var/www/owncloud
    </Directory>
    ```

    7.3. Enable the Virtual Host Configuration

    ```
    a2ensite owncloud.conf
    systemctl reload apache2
    ```

    7.4. Configure the Database

    ```
    mysql -u root -e "CREATE DATABASE IF NOT EXISTS owncloud; \
    GRANT ALL PRIVILEGES ON owncloud.* \
    TO owncloud@localhost \
    IDENTIFIED BY 'password'";
    ```

    7.5. Enable the Recommended Apache Modules

    ```
    a2enmod dir env headers mime rewrite setenvif
    systemctl reload apache2
    ```

8. **Configure ownCloud**

    8.1. Install ownCloud by creating the first user (admin user)

    ```
    occ maintenance:install \
        --database "mysql" \
        --database-name "owncloud" \
        --database-user "owncloud" \
        --database-pass "password" \
        --admin-user "adegaillier" \
        --admin-pass "<admin-password>"
    ```

    8.2. Configure ownCloud’s Trusted Domains

    ```
    occ config:system:set trusted_domains 1 --value="<ip-address>"
    ```

    8.3. Set Up a Cron Job

    ```
    echo "*/15  *  *  *  * /usr/bin/php -f /var/www/owncloud/cron.php" \
    > /var/spool/cron/crontabs/www-data
    chown www-data.crontab /var/spool/cron/crontabs/www-data
    chmod 0600 /var/spool/cron/crontabs/www-data
    ```

    8.4. Configure Log Rotation in /etc/logrotate.d/owncloud

    ```
    /var/www/owncloud/data/owncloud.log {
    size 10M
    rotate 12
    copytruncate
    missingok
    compress
    compresscmd /bin/gzip
    }
    ```

    8.5. Finalise the Installation (Didn't do that -> worth less)

    ```
    FILE="/usr/local/bin/ocpermissions"

    /bin/cat <<EOM >$FILE
    #!/bin/bash

    ocpath="{install-directory}"
    datadir="{install-directory}/data"
    htuser="{webserver-user}"
    htgroup="{webserver-group}"
    rootuser="root"

    printf "Creating any missing directories"
    sudo -u "${htuser}" mkdir -p "$ocpath/assets"
    sudo -u "${htuser}" mkdir -p "$ocpath/updater"
    sudo -u "${htuser}" mkdir -p "$datadir"

    printf "Update file and directory permissions"
    sudo find "${ocpath}/" -type f -print0 | xargs -0 chmod 0640
    sudo find "${ocpath}/" -type d -print0 | xargs -0 chmod 0750

    printf "Set web server user and group as ownCloud directory user and group"
    sudo chown -R "${rootuser}:${htgroup}" "${ocpath}/"
    sudo chown -R "${htuser}:${htgroup}" "${ocpath}/apps/"
    sudo chown -R "${htuser}:${htgroup}" "${ocpath}/apps-external/"
    sudo chown -R "${htuser}:${htgroup}" "${ocpath}/assets/"
    sudo chown -R "${htuser}:${htgroup}" "${ocpath}/config/"
    sudo chown -R "${htuser}:${htgroup}" "${datadir}"
    sudo chown -R "${htuser}:${htgroup}" "${ocpath}/updater/"
    sudo chmod +x "${ocpath}/occ"

    printf "Set web server user and group as .htaccess user and group"
    if [ -f "${ocpath}/.htaccess" ]; then
    sudo chmod 0644 "${ocpath}/.htaccess"
    sudo chown "${rootuser}:${htgroup}" "${ocpath}/.htaccess"
    fi

    if [ -f "${datadir}/.htaccess" ]; then
    sudo chmod 0644 "${datadir}/.htaccess"
    sudo chown "${rootuser}:${htgroup}" "${datadir}/.htaccess"
    fi

    EOM

    # Make the script executable
    sudo chmod +x /usr/local/bin/ocpermissions

    ocpermissions
    ```

9. Change location of the owncloud data folder (root)

    9.1. Create directory in your hard drive

    ```
    mkdir /media/<your_hard_drive>/owncloud
    ```

    9.2. Stop the webserver apache

    ```
    systemctl stop apache2
    ```

    9.3. Copy all current files of the owncloud data in your new location and rename previous location to avoid confusion

    ```
    cp -rT /var/www/owncloud/data/ /media/user/<your_hard_drive>/owncloud/
    mv /var/www/owncloud/data /var/www/owncloud/data_orig
    ```

    9.4. Apply the proper group and read/write permissions onto the hard drive’s data directory. **See below for permanent change for group and permissions of the hard drive’s data directory**
    ```
    chown -R www-data:www-data /media/user/<your_hard_drive>/owncloud/
    chmod -R 0770 /media/user/<your_hard_drive>/owncloud/
    ```

    9.5. Create the folder that will be mounted in the previous owncloud data location and change to the proper group and permissions as well.

    ```
    mkdir /var/www/owncloud/data
    chown -R www-data:www-data /var/www/owncloud/data
    chmod -R 0770 /var/www/owncloud/data
    ```

    9.6. mount bind the two locations

    ```
    mount --bind /media/user/<your_hard_drive>/owncloud/ /var/www/owncloud/data/
    ```

    9.7. Start webserver
    ```
    systemctl stop apache2
    ```

Owncloud must be the owner of the directory data and all subdirectories and files into it. The permissions of the directory data are restricted to all users execpt the owner and root.  As the data are on an external memory device, the best permanent way to do this is to mount the media with the correct umask and user/group ID.

Modify /etc/fstab to mount correctly the media and to bind the location of your cloud to the default location of the owncloud data directory:

```
UUID=AADEEA03DEE9C7A1 	/media/Toshiba 	ntfs 	defaults,uid=www-data,gid=www-data,umask=007 	0 	0
/media/Toshiba/owncloud	/var/www/owncloud/data	none	defaults,bind	0	0
```

```
chown www-data:www-data <your-directory-path>
chmod 750 <your-directory-path>
```

10. Access ownCloud with SSL (https)

The default installation of ownCloud is not secured by SSL. To enable SSL in your webserver, run these commands:

```
a2enmod ssl
a2ensite default-ssl
systemctl reload apache2
```

**References**

- [https://doc.owncloud.com/server/admin_manual/installation/deployment_considerations.html](https://doc.owncloud.com/server/admin_manual/installation/deployment_considerations.html)
- [https://buzut.net/configuration-dun-serveur-linux-apache2](https://buzut.net/configuration-dun-serveur-linux-apache2/#perf)
- [https://buzut.net/configuration-dun-serveur-linux-php](https://buzut.net/configuration-dun-serveur-linux-php/)
- [https://doc.owncloud.com/server/admin_manual/installation/ubuntu_18_04.html](https://doc.owncloud.com/server/admin_manual/installation/ubuntu_18_04.html)
- [https://manandkeyboard.com/2014/09/15/owncloud-and-an-external-hard-drive/](https://manandkeyboard.com/2014/09/15/owncloud-and-an-external-hard-drive/)
- [https://www.howtoforge.com/tutorial/owncloud-install-debian-8-jessie/](https://www.howtoforge.com/tutorial/owncloud-install-debian-8-jessie/)
- [https://foxty.io/passer-owncloud-https/](https://foxty.io/passer-owncloud-https/)


## Owncloud - Docker installation
Once your ubuntu server is ready for owncloud, you must install docker by following the official steps related to the ubuntu distro here: https://docs.docker.com/engine/install/

**Note:**
For RPI <= 3, the architecture is aarch64, you must install the sources from the arm64 architecture. (Ubuntu 20.04 LTS focal)

Once docker engine is running hello world test container, you can install docker compose : https://docs.docker.com/compose/install/

* Test Docker installation
    ```
    docker run hello-world
    ```
-> cannot install docker compose from github sources for Raspberry pi 3 architecture (ARM). It's only available for x86 architecture... -_-

You must install it throught python3: https://dev.to/rohansawant/installing-docker-and-docker-compose-on-the-raspberry-pi-in-5-simple-steps-3mgl

* IMPORTANT! Install proper dependencies
    ```
    sudo apt-get install -y libffi-dev libssl-dev && sudo apt-get install -y python3 python3-pip
    ```
    ```
    sudo apt-get remove python-configparser
    ```

* Install Docker Compose
    ```
    sudo pip3 -v install docker-compose
    ```

* Validate the installation with

```
docker-compose --version
```

Now that everything is ready to install owncloud via docker. First, download on the device the docker compose file and environment variable file provided by the repository https://gitlab.com/adegaillier/owncloud-docker.

1. Download docker compose file and environment variable file
    ```
    wget https://gitlab.com/adegaillier/owncloud-docker/-/raw/master/.env
    wget https://gitlab.com/adegaillier/owncloud-docker/-/raw/master/docker-compose.yml
    wget https://gitlab.com/adegaillier/owncloud-docker/-/raw/master/Dockerfile
    ```
2. Set the `OWNLCOUD_FILES_LOCATION` in the .env file to the target location for the files storage. fex: `OWNLCOUD_FILES_LOCATION=/media/RAID/`
3. Build the docker project
    ```
    sudo docker build --force-rm -t docker-owncloud:latest .
    ```
4. Launch the Owncloud docker service and follow starting logs. UI web is ready once apache2 daemon is running.
    ```
    sudo docker-compose up --remove-orphans -d
    ```
    ```
    sudo docker-compose logs --follow owncloud
    ```
5. Create a new admin user and remove the default one via the web UI to be able to redeploy the service with the previous volumes. Otherwise, you must remove all the docker volume to be able to restart the container.

**Notes:**
- You must have the www-data ownership for the location given by `OWNLCOUD_FILES_LOCATION`. In case of an external drive, the only solution I got is to mount the entire partition inside the external drive with the specified uid and gid of the user and group `www-data`.
    - Go to /etc/fstab to append a mounting rule for the UUID of the partition/drive. It works for **ntfs partition only**:
        ```
        UUID=D6DA9AB0DA9A8BFD /media/USB-drive ntfs defaults,uid=33,gid=33,umask=007 0 0
        ```
- The container will automatically start after a reboot because of the `restart: always` option inside the 3 services declared in the docker-compose file. If you want to "factory reset" the owncloud docker instance, you just need to stop the container, remove volumes and restart it:
    ```
    sudo docker-compose down --remove-orphans -v
    ```
    ```
    sudo docker-compose up --remove-orphans -d
    ```
- Execute command in docker
    ```
    docker exec ${container_name} ${cmd}
    ```
- Scan database of owncloud while running in container
    ```
    docker exec owncloud-docker_owncloud_1 occ files:scan --all
    ```

Now everything is working correctly, you can use owncloud as you which!


**ref:**
- [https://hub.docker.com/_/owncloud](https://hub.docker.com/_/owncloud)
- [https://doc.owncloud.com/server/admin_manual/installation/docker/](https://doc.owncloud.com/server/admin_manual/installation/docker/)
- [Docker compose overview](https://docs.docker.com/compose/)
- [Cleaning docker containers, images, volumes and network](https://linuxize.com/post/how-to-remove-docker-images-containers-volumes-and-networks/#:~:text=To%20remove%20one%20or%20more,containers%20you%20want%20to%20remove.&text=If%20you%20get%20an%20error,the%20container%20before%20removing%20it.)
- [Start your container automatically](https://mehmandarov.com/start-docker-containers-automatically/)

## Owncloud - synch google drive

[https://doc.owncloud.com/server/admin_manual/configuration/files/external_storage/google.html](https://doc.owncloud.com/server/admin_manual/configuration/files/external_storage/google.html)


## Owncloud - disable checking permission on data directory

In /var/www/owncloud logging in root, the output of

```
grep "readable by other" `find . -name '*.php'`
```

Gives

```
./lib/private/legacy/util.php: 'error' => $l->t('Your Data directory is readable by other users'),
```

Modify the function checkDataDirectoryPermissions() into util.php to return always "OK". Modify the return statment in line 962 from

```
return $errors;
```

to

```
return 0;
```





**Reference**

- [https://forum.owncloud.org/viewtopic.php?t=25043](https://forum.owncloud.org/viewtopic.php?t=25043)


