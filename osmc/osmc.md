# Osmc server

## Setup static ip address

1. use connmanctl to configure the static ip address through your connected service (here ethernet) **!! DNS isn't working if the ip address of the ethernet interface is static with this method. I recommend to use the router association ip - mac address !!**

```
# connmanctl
connmanctl> config ethernet_b827eb5cf9b2_cable --ipv4 manual 192.168.0.19 255.255.255.0 192.168.0.1
```

**References**

- [https://www.mankier.com/1/connmanctl](https://www.mankier.com/1/connmanctl)