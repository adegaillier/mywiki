# Installing pipenv

You must have already installed python on your OS. (Preferably python3)

```
pip3 install pipenv
```

or

```
sudo apt install pipenv
```

# Create and manage virtual python environment

Then you can start a new local virtual python environment for any project:


```
pipenv shell
```

To add new required package with specific version:

```
pipenv install numpy==1.19.4
```

To add new package for development purpose only:

```
pipenv install pytest --dev
```

Lock you development environment for production freeze:

```
pipenv lock
```

# Install recorded environment

Install environment locked (Pipenv will only create environment based on Pipfile.lock files):

```
pipenv install --ignore-pipfile
```

Install dev environment:

```
pipenv install --dev
```

Install current environment:

```
pipenv install
```


#### References

pipenv-guide: [https://realpython.com/pipenv-guide/](https://realpython.com/pipenv-guide/)