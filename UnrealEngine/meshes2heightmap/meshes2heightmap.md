Here I will explain you a method to convert 3D meshes into 2D heightmap that can be use to create landscape into Unreal Engine for example.

We will use Blender to do that.


1. Open mesh into Blender

1. Align mesh to the origin and rise it above the 0 ground

1. Add a camera actor and set the following configuration to the camera (based on the mesh you want to capture)

Output properties:
  - Resolution X: 2048
  - Resolution Y: 2048
  - Color: BW
  - Color depth: 16

Object properties:
  - Set the height above the mesh to be able to see it ;)

Object data properties:
  - Type: Orthographic
  - Orthographoc scale & shift XY: adapt to fit the mesh to the camera FOV

![setting camera](assets/setting_camera.gif)

1. Render a first image then open the Compositing tab and tick the box "use Nodes".

1. Now we will add two nodes, one to normalize the camera vue based on the depth and a second to invert the normalization to make it correspond to black (deeper) and white (higher).

![compositing](assets/compositing.gif)

1. Now you can render and export the 2D heighmap in PNG or JPG.